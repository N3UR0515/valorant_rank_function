# valorant_rank_function

All credit goes to [@kyroskoh](https://linktr.ee/kyroskoh) for the original code. I just made it into a python function.

## Usage

!note: You may need to change the region specified in the code to match your own. For example change `eu` to `na` if you are in North America.

git pull this repo and import the function into your code.

get_valorant_rank(Username#Tagline)
