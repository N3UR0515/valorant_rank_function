import requests
from requests.exceptions import HTTPError

def get_valorant_rank(username: str) -> str:
    '''
    Retrieve the Valorant rank for a specified user.

    This function sends a request to an API service to obtain the Valorant rank associated with the given username and tagline.
    The 'username' parameter is a single string that combines the player's username and their tagline in Valorant, separated by a '#'. The format should be 'username#tagline'.

    Parameters:
    username (str): A string combination of user and tagline separated by a '#', representing the Valorant username and tagline, formatted as 'username#tagline'.

    Returns:
    str: The rank of the user in Valorant, or a message indicating an error if the request fails.

    Raises:
    HTTPError: If an HTTP error occurs during the request.
    '''
    try:
        # Splitting the username and tagline
        user = username.split("#")[0]
        tagline = username.split("#")[1]
        response = requests.get(f'https://api.kyroskoh.xyz/valorant/v1/mmr/eu/{user}/{tagline}?display=0')
        response.raise_for_status()
        rank = response.text  # Placeholder for actual response parsing
        print(rank)
        return rank
    except HTTPError as http_err:
        return f"HTTP error occurred: {http_err}"
    except Exception as err:
        return f"An error occurred: {err}"

# Example usage
# get_valorant_rank("Username#Tagline")
